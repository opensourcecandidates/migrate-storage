import {StorageMigration, Adapter, MongodbAdapter} from '../src';

describe('Update storage', () => {

    this.initAdapter = (currVer: Number) => {
        MongodbAdapter.prototype.setVersion = jest.fn();
        MongodbAdapter.prototype.getCurrentVersion = jest.fn(() => new Promise(resolve => resolve(currVer)));
    }

    this.clearMocks = () => {
        for(let i = 0; i < 5; i++) {
            this.migrations[i].up.mockClear();
        }
    }

    beforeAll(() => {
        this.initAdapter(0);
    })
   
    beforeEach(() => {
        this.migrations = [
            {up: jest.fn(), down: jest.fn()},
            {up: jest.fn(), down: jest.fn()},
            {up: jest.fn(), down: jest.fn()},
            {up: jest.fn(), down: jest.fn()},
            {up: jest.fn(), down: jest.fn()},
        ];
       this.adapter = new MongodbAdapter('');
    })

    afterEach(() => {
        this.clearMocks();
    });

    it('Should update version by number of migrations', async () => {
        const subject = new StorageMigration(this.adapter, this.migrations);
        await subject.updateStorage();
        expect(this.adapter.setVersion).toBeCalledWith(5);
    });

    it('Should call', async () => {
        const subject = new StorageMigration(this.adapter, this.migrations);
        await subject.updateStorage();
        expect(this.adapter.setVersion).toBeCalledWith(5);
    });

    it('Should never call to down', async () => {
        for(let i = 0; i < 5; i++) {
            this.initAdapter(i);
            const subject = new StorageMigration(this.adapter, this.migrations);
            await subject.updateStorage();
            for(let m = 0; m < 5; m++) {
                expect(this.migrations[m].down).not.toBeCalled();
            }
        }
    });

    it('Should call only relevant up()', async () => {
        this.initAdapter(0);
        let subject = new StorageMigration(this.adapter, this.migrations);
        await subject.updateStorage();
        for(let i = 0; i < 5; i++) {
            expect(this.migrations[i].up).toBeCalledTimes(1);
        }

        this.clearMocks();
        this.initAdapter(1);
        subject = new StorageMigration(this.adapter, this.migrations);
        await subject.updateStorage();
        expect(this.migrations[0].up).not.toBeCalled();
        expect(this.migrations[1].up).toBeCalledTimes(1);
        expect(this.migrations[2].up).toBeCalledTimes(1);
        expect(this.migrations[3].up).toBeCalledTimes(1);
        expect(this.migrations[4].up).toBeCalledTimes(1);

        this.clearMocks();
        this.initAdapter(2);
        subject = new StorageMigration(this.adapter, this.migrations);
        await subject.updateStorage();
        expect(this.migrations[0].up).not.toBeCalled();
        expect(this.migrations[1].up).not.toBeCalled();
        expect(this.migrations[2].up).toBeCalledTimes(1);
        expect(this.migrations[3].up).toBeCalledTimes(1);
        expect(this.migrations[4].up).toBeCalledTimes(1);

        this.clearMocks();
        this.initAdapter(3);
        subject = new StorageMigration(this.adapter, this.migrations);
        await subject.updateStorage();
        expect(this.migrations[0].up).not.toBeCalled();
        expect(this.migrations[1].up).not.toBeCalled();
        expect(this.migrations[2].up).not.toBeCalled();
        expect(this.migrations[3].up).toBeCalledTimes(1);
        expect(this.migrations[4].up).toBeCalledTimes(1);

        this.clearMocks();
        this.initAdapter(4);
        subject = new StorageMigration(this.adapter, this.migrations);
        await subject.updateStorage();
        expect(this.migrations[0].up).not.toBeCalled();
        expect(this.migrations[1].up).not.toBeCalled();
        expect(this.migrations[2].up).not.toBeCalled();
        expect(this.migrations[3].up).not.toBeCalled();
        expect(this.migrations[4].up).toBeCalledTimes(1);

        this.clearMocks();
        this.initAdapter(5);
        subject = new StorageMigration(this.adapter, this.migrations);
        await subject.updateStorage();
        expect(this.migrations[0].up).not.toBeCalled();
        expect(this.migrations[1].up).not.toBeCalled();
        expect(this.migrations[2].up).not.toBeCalled();
        expect(this.migrations[3].up).not.toBeCalled();
        expect(this.migrations[4].up).not.toBeCalled();

    });
});

describe('setVersion', () => {
    this.initAdapter = (currVer: Number) => {
        MongodbAdapter.prototype.setVersion = jest.fn();
        MongodbAdapter.prototype.getCurrentVersion = jest.fn(() => new Promise(resolve => resolve(currVer)));
    }

    this.clearMocks = () => {
        for(let i = 0; i < 5; i++) {
            this.migrations[i].up.mockClear();
        }
    }

    beforeAll(() => {
        this.initAdapter(0);
    })
   
    beforeEach(() => {
        this.migrations = [
            {up: jest.fn(), down: jest.fn()},
            {up: jest.fn(), down: jest.fn()},
            {up: jest.fn(), down: jest.fn()},
            {up: jest.fn(), down: jest.fn()},
            {up: jest.fn(), down: jest.fn()},
        ];
       this.adapter = new MongodbAdapter('');
    })

    afterEach(() => {
        this.clearMocks();
    });

    it('should run only up() when setVersion 5 from 0', async () => {
        const subject = new StorageMigration(this.adapter, this.migrations);
        await subject.setVersion(5);
        for(let i; i < 5 ; i++) {
            expect(this.migrations[i].up).toBeCalledTimes(1);
            expect(this.migrations.down).not.toBeCalled();
        }
    });

    it('should run only down() when downgrading from last to 0', async () => {
        this.initAdapter(5);
        const subject = new StorageMigration(this.adapter, this.migrations);
        await subject.setVersion(0);
        for(let i; i < 5 ; i++) {
            expect(this.migrations[i].down).toBeCalledTimes(1);
            expect(this.migrations.up).not.toBeCalled();
        }
    });

    it('should run only relevant up', async () => {
        this.initAdapter(3);
        const subject = new StorageMigration(this.adapter, this.migrations);
        await subject.setVersion(4);
        for(let i = 0 ; i < 5; i++) {
            expect(this.migrations[i].down).not.toBeCalled();
        }
        expect(this.migrations[0].up).not.toBeCalled();
        expect(this.migrations[1].up).not.toBeCalled();
        expect(this.migrations[2].up).not.toBeCalled();
        expect(this.migrations[3].up).toBeCalledTimes(1);
        expect(this.migrations[4].up).not.toBeCalled();
    });
});
