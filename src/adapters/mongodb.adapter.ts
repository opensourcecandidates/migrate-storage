import { Adapter } from ".";
import {Typegoose, prop} from 'typegoose';
import * as mongoose from 'mongoose';

class Schema extends Typegoose {
    @prop({required: true})
    ver: number;
}

const schemaModel =  new Schema()
  .getModelForClass(Schema);

export class MongodbAdapter implements Adapter {
    constructor(private dbPath: string) {}

    private async getSchema() {
        // await mongoose.connect(this.dbPath,{
        //     useNewUrlParser: true
        //   });
        const schemas = await schemaModel.find();
        if(schemas && schemas[0]) {
            return schemas[0];
        }
        return null;
    }

    async getCurrentVersion() {
        const schema = await this.getSchema();
//        await mongoose.connection.close();
        return schema ? schema.ver : 0;
    }

    async setVersion(ver: number) {
        const schema = await this.getSchema();
        if(schema) {
            schema.ver = ver;
            await schema.save();
        } else {
            await schemaModel.create({ver});
        }
//        await mongoose.connection.close();
    }
}