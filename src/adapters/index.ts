export interface Adapter {
    getCurrentVersion(): Promise<number>;
    setVersion(ver: number): Promise<void>;
}

export * from './mongodb.adapter';
