import { Adapter } from './adapters';
export { Adapter, MongodbAdapter } from './adapters';

export interface Migration {
    up(): Promise<void>
    down(): Promise<void>
}

export class StorageMigration {
    constructor(private adapter: Adapter, private migrations: Migration[]) {}

    private get targetVer() {return this.migrations.length} 

    async updateStorage() {
        const currVer = await this.adapter.getCurrentVersion();
        const targetVer = this.migrations.length;
        if(targetVer > currVer) {
            for(let i = currVer; i < targetVer; i++) {
                await this.migrations[i].up();
            }
            await this.adapter.setVersion(targetVer);
        }
    }

    async setVersion(ver: number) {
        if(ver == this.targetVer) {
            return this.updateStorage();
        }
        if(ver < this.targetVer) {
            const currVer = await this.adapter.getCurrentVersion();
            if(ver > currVer) {
                for(let i = currVer; i < ver; i++) {
                    await this.migrations[i].up();
                }
            } else {
                for(let i = currVer - 1; i >= ver; i--) {
                    await this.migrations[i].down();
                }
            }
        }
    }
}
